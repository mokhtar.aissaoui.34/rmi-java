//////***********Fait par :   
          
///**************************Aissaoui EL mokhtar
///**************************Imane Es-ssaber

package Client;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;

import SERVEUR.*;




public class Client implements IClient  {
	
	public Client() throws RemoteException {
		super();
		UnicastRemoteObject.exportObject(this, 0);
    }

	@Override
	public void alert(int n) throws RemoteException {
		 System.out.println("Attention on a d�passer les "+n+" patient");	
	}

	
	
	public static void main(String args[]) throws RemoteException  {
	IClient c = new Client();
		  System.out.println("Lancement du client");
		  String host = (args.length < 1) ? null : args[0];
		  System.setProperty("java.security.policy","C:\\Users\\marcb\\eclipse-workspace\\RmiClient\\bin\\Client\\java.policy" );
			System.setSecurityManager (new SecurityManager());
		try {
			
			Registry registry = LocateRegistry.getRegistry(host);
		VeterinaireI stub = (VeterinaireI) registry.lookup("Veterinaire");
		stub.registerClient(c);
		int cmd =1;
		Scanner sc = new Scanner(System.in);
		while(cmd != 0){
			
			System.out.println("Bonjour,que voullez vous faire ? \n"
					+ "1: Afficher tout les patients. \n"
					+ "2 : ajout� un patients.\n"
					+ "3 : SUpprim� un patient.\n"
					+ "4 : R�cuperer les infos d'un patient.\n"
					+ "0: Quit"
					);
			cmd = sc.nextInt();
			
			switch(cmd){
			case 0:
				stub.disconnectClient(c);
				System.out.println("Vous avez quittez le serveur !");
				System.exit(0);
			case 1:
				System.out.println(stub.printAllPatient());
			break;
			case 2:
				String Nom ="";
				String Maitre ="";
				String sp="";
				String breed="";
				
				Scanner sc2 = new Scanner(System.in);

				System.out.println("Le nom de l'animale?");
				Nom = sc2.nextLine();
				System.out.println("Nom du maitre ?");
				Maitre = sc2.nextLine();
				System.out.println("l'esp�ce de l'animal ?");
				breed = sc2.nextLine();
				System.out.println("la race de l'animal ?");
				sp = sc2.nextLine();
				if(stub.contient(Nom)) {
					System.out.println("ce patient existe d�ja !");
					break;
				}
				
				stub.add(Nom,Maitre,breed,sp);
			break;
			case 3:
				Scanner sc3 = new Scanner(System.in);
				System.out.println("Entr� le nom de l'animal � supprim�  : ");
				String name2 = sc3.nextLine();
				stub.remove(name2);
			break;
			case 4:
				
				Scanner sc6 = new Scanner(System.in);
				System.out.println("Entr� le nom de l'animal � afficher : ");
				String name4 = sc6.nextLine();
				System.out.println(stub.getPatient(name4).printAnimal());
				break;
			default:
			System.out.println("Erreur");

			System.out.println("Commands available :"
					+ "1: Afficher tout les patients. \n"
					+ "2 : ajout� un patients."
					+ "3 : SUpprim� un patient."
					+ "4 : R�cuperer les infos d'un patient."
					+ "0: Quit"
					);
			}
			
		}
		} catch (Exception e) {/*!< error cases*/
			System.err.println("Client exception: " + e.toString());
			e.printStackTrace();
		}
	}
	}

