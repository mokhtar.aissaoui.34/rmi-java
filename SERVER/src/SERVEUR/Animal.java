package SERVEUR;
//////***********Fait par :   

///**************************Aissaoui EL mokhtar
///**************************Imane Es-ssabers
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface Animal extends Remote{

	String	getFullName() throws RemoteException ;
// R�cuperer le nom de l'animale 
	String getNameMaster() throws RemoteException;
// R�cuperer le nom du maitre
	String getSpecies() throws RemoteException;
	//recup�rer l'espece de l'animale
	String getBreed() throws RemoteException;
 	// la race
	DossierPatientI getDossier() throws RemoteException;
	String printAnimal() throws RemoteException;

}
