package SERVEUR;
//////***********Fait par :   

///**************************Aissaoui EL mokhtar
///**************************Imane Es-ssaber
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Animalimp extends UnicastRemoteObject implements Animal {
	protected String name;
	protected String masterName;
	protected String breed;
	protected String species;
	protected DossierPatientI dossier;
	
	public Animalimp (String name, String masterName,String breed ,String species,DossierPatientI d) throws RemoteException {
		super();
		this.name = name;
		this.masterName = masterName;
		this.breed = breed;
		this.species = species;
		this.dossier = d;
	}

	@Override
	public String getFullName() throws RemoteException {
		// TODO Auto-generated method stub
		return this.name;	
	}

	@Override
	public String getNameMaster() throws RemoteException {
		// TODO Auto-generated method stub
		return this.masterName;
	}

	@Override
	public String getSpecies() throws RemoteException {
		// TODO Auto-generated method stub
		return this.species;
	}

	@Override
	public String getBreed() throws RemoteException {
		// TODO Auto-generated method stub
		return this.breed;
	}

	@Override
	public String printAnimal() throws RemoteException {
		// TODO Auto-generated method stub
	String res ="Nom : " + this.getFullName()
		+"\n             - Maitre : " + this.getNameMaster()
		+"\n             - Esp�ce : " + this.getSpecies()
		+"\n             -Prochain RDV : "+ this.dossier.getrdv();
	return res;
		
		
	}

	public DossierPatientI getDossier() throws RemoteException {
		return this.dossier;
	}

}
