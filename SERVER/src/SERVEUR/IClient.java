package SERVEUR;
//////***********Fait par :   

///**************************Aissaoui EL mokhtar
///**************************Imane Es-ssaber
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IClient extends Remote{
    void alert(int n) throws RemoteException;
}
