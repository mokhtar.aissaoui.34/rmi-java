package SERVEUR;
//////***********Fait par :   

///**************************Aissaoui EL mokhtar
///**************************Imane Es-ssaber
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class Veterinaire extends UnicastRemoteObject  implements VeterinaireI {
	protected ArrayList<Animal> patients;
	protected ArrayList<IClient> cr;
	
	public Veterinaire() throws RemoteException{
		super();
		patients =new ArrayList<Animal>();
		cr = new ArrayList<IClient>();
	}
public boolean contient(String nom) throws RemoteException {
	boolean R = false;
	for(int i=0;i<this.patients.size();i++) {
		if(this.patients.get(i).getFullName().equals(nom)) {
			R = true;
			break;
		}
	}
	return R;
}
	@Override
	public Animal getPatient(String name) throws RemoteException {
		if(this.contient(name)) {
			for(int i=0;i<this.patients.size();i++) {
				if(this.patients.get(i).getFullName().contentEquals(name)) {
					return  this.patients.get(i);
				}
			}
		}
		else {
			System.out.println("Aucun patient � ce nom");
			
			return null;
		}
		return null;
			
		
	}

	@Override
	public VeterinaireI add(String name, String masterName,String breed ,String species) throws RemoteException 
	{
		System.out.println("test");
		if(this.contient(name)) {
			System.out.println("ce patient existe d�ja !");
		}
		else
		{	Animalimp a = new Animalimp (name,masterName,breed,species, new DossierPatient() );
			this.patients.add(a);
			System.out.println("Patient Ajout�e !!");
		if (this.getSize() >= 100 && this.getSize()< 500 ) {
			this.alertAllClients(100);
			
		}
		if (this.getSize() >= 500 && this.getSize()< 1000 ) {
			this.alertAllClients(500);
			
		}
		if (this.getSize() >= 1000 ) {
			this.alertAllClients(1000);
			
		}
		
		}
		return this;
		
	}

	@Override
	public VeterinaireI remove(String a) throws RemoteException {
		if(this.contient(a)) 
		{
			for(int i = 0; i < this.patients.size(); i++) 
			{
				if ( this.patients.get(i).getFullName().contentEquals(a) ) 
				{
					this.patients.remove(i);
					break;
				}
			}
		}else {
			System.out.println("Patient introuvable !!");
		}
		return this;
	}

	@Override
	public String printAllPatient() throws RemoteException {
	String res =	"============ | Collection de patients ("+this.patients.size()+"): | ============\n";
		for(int i = 0; i< this.patients.size();i++) {
			res += "============ | "+i+" : "+this.patients.get(i).getFullName()+" , Maitre : "+this.patients.get(i).getNameMaster()+"\n";
		}
		return res;
		
	}

	@Override
	public int getSize() throws RemoteException {
		// TODO Auto-generated method stub
		return this.patients.size();
	}
	
	public void registerClient(IClient client) throws RemoteException {
		this.cr.add(client);
		System.out.println("Client enregistr� : " + client.toString());
	}
	
	
	public void disconnectClient(IClient client) throws RemoteException {
		this.cr.remove(client);
		System.out.println("Client d�connect� :  " + client.toString());
	}
	
	public void alertAllClients(int n) throws RemoteException {
		for(IClient c : this.cr ) {
				c.alert(n);
		}
	}


}
