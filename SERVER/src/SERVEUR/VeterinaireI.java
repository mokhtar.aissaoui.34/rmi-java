package SERVEUR;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface VeterinaireI extends Remote {

	public Animal getPatient(String name) throws RemoteException;

	
 public	VeterinaireI add(String name, String masterName,String breed ,String species) throws RemoteException;
	
	public VeterinaireI remove(String a) throws RemoteException;
	
  public	String printAllPatient() throws RemoteException;
	
	
	 public int getSize() throws RemoteException;
	 public void registerClient(IClient client) throws RemoteException ;
	 public void disconnectClient(IClient client) throws RemoteException ;
	 public void alertAllClients(int n) throws RemoteException;
	
	 public boolean contient(String nom) throws RemoteException;
}
